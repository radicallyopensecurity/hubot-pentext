# FROM rocketchat/hubot-rocketchat:latest
FROM registry.gitlab.com/radicallyopensecurity/hubot-rocketchat:latest

MAINTAINER Radically Open Security <infra@radicallyopensecurity.com>

USER root

ENV LANG C.UTF-8 \
    LC_CTYPE C.UTF-8 \
    BOT_NAME "rosbot" \
    PENTEXT_PATH "/home/hubot/pentext"

## BEGIN INSTALL JAVA, PYTHON & PENTEXT

RUN apk add --upgrade apk-tools

RUN echo "http://dl-8.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories\
        && apk add --update --no-cache \
        openjdk8 \
        bash \
        python3 \
        python3-dev \
        # pandoc \
        # openblas-dev \
        ca-certificates \
        openssl \
        libgfortran=6.3.0-r4 \
        gfortran=6.3.0-r4 \
        lapack-dev=3.7.0-r0 \
        gcc  \
        py3-pip \
        build-base \
        wget \
        curl \
        openssh-client \
        freetype-dev \
        libxml2-dev \
        libxslt-dev \
        libpng-dev && \
        python3 -m ensurepip && \
        rm -r /usr/lib/python*/ensurepip && \
        pip3 install --upgrade pip setuptools && \
        if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
        if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
        rm -r /root/.cache && \
        ln -s /usr/include/locale.h /usr/include/xlocale.h

RUN wget https://github.com/jgm/pandoc/releases/download/2.2.3.1/pandoc-2.2.3.1-linux.tar.gz && \
        tar zxvf pandoc-2.2.3.1-linux.tar.gz && \
        mv pandoc-2.2.3.1/bin/pandoc /usr/bin && \
        mv pandoc-2.2.3.1/bin/pandoc-citeproc /usr/bin && \
        rm -rf pandoc-2.2.3.1*

## END INSTALL JAVA, PYTHON & PENTEXT

## BEGIN SETUP DOCBUILDER

# Setup Saxon

RUN curl https://downloads.sourceforge.net/project/saxon/Saxon-HE/9.7/SaxonHE9-7-0-6J.zip -o /tmp/saxon.zip -L \
    && mkdir /tmp/saxon && cd /tmp/saxon \
    && unzip /tmp/saxon.zip \
    && mkdir -p /usr/local/bin/saxon \
    && mv /tmp/saxon/saxon9he.jar /usr/local/bin/saxon/saxon9he.jar

## END SETUP DOCBUILDER

RUN pip3 install -U setuptools
ADD requirements.txt /tmp/requirements.txt

RUN apk add --no-cache --virtual .build-deps mariadb-dev \
    && pip3 install -r /tmp/requirements.txt \
    && apk add --virtual .runtime-deps mariadb-client-libs \
    && apk del .build-deps


USER hubot

WORKDIR /home/hubot
